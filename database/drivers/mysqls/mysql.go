package mysqls

import (
	"database/sql"
	"fmt"
	"github.com/thrasher-corp/gocryptotrader/database"
)

func Connect(cfg *database.Config) (*database.Instance, error) {
	connect := fmt.Sprintf(
		"%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=UTC",
		cfg.Username,
		cfg.Password,
		cfg.Host,
		cfg.Database,
	)
	db, err := sql.Open(database.DBMySQL, connect)
	if err != nil {
		return nil, err
	}
	err = database.DB.SetMySQLConnection(db)
	if err != nil {
		return nil, err
	}
	return database.DB, nil
}
